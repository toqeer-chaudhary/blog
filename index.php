<?php
        include "assets/includes/header.php";
        include "assets/includes/navbar.php";
        include "assets/includes/carousel.php";

?>

<!-- Modal for users-->
<div class="modal fade" id="userModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">User Registeration</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="" method="post" id="user-form">
                    <div class="row">
                        <div class="form-group col-sm-6">
                            <label class="text-inverse" for="first_name">First Name</label>
                            <input type="text" class="form-control" id="first-name" placeholder="John" name="first_name">
                            <p id="first-name-error"></p>
                        </div>
                        <div class="form-group col-sm-6 ml-auto">
                            <label class="text-inverse" for="email">Email</label>
                            <input type="text" class="form-control" id="email" placeholder="john@xyz.com" name="email">
                            <p id="email-error"></p>
                        </div>
                        <div class="modal-footer col-sm-12">
                            <button type="submit" class="btn btn-outline-dark" id="submit-user">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Modal for users end-->

<!-- Modal for Posts begin-->

<div class="modal fade" id="postModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Create A Post</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="" method="post" id="post-form">
                    <div class="row">
                        <div class="form-group col-sm-6">
                            <label class="text-inverse" for="post_title">Post Title</label>
                            <input type="text" class="form-control" id="post-title" placeholder="A Great World" name="post_title">
                            <p id="post-title-error"></p>
                        </div>
                        <div class="form-group col-sm-6 ml-auto">
                            <label for="post_categories">Select Categories:</label>
                            <select class="form-control" id="post-categories" name="post_categories">
<!--                                <option value="">Please Select User</option>-->
<!--                                <option value="JavaScript">JavaScript</option>-->
<!--                                <option value="Bootstrap">Bootstrap</option>-->
<!--                                <option value="PHP">PHP</option>-->
<!--                                <option value="HTML">HTML</option>-->
                            </select>
                            <p id="post-categories-error"></p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="text-inverse" for="post_descriptioon">POST Description</label>
                        <textarea class="form-control" rows="5" id="post-descriptioon" name="post_description" placeholder="Description"></textarea>
                        <p id="post-description-error"></p>
                    </div>
                    <div class="modal-footer col-sm-12">
                        <button type="submit" class="btn btn-outline-dark" id="create-post">Create Post</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Modal for Posts end-->

<!-- Blog Posts Begin -->
<div class="container mt-5">
    <h2 class="my-4 text-center bg-dark text-light">BLOGS Are Awesome</h2>
    <div class="row">
        <div class="col-sm-8">
            <!-- Blog Post -->
            <div class="card mb-4">
                <div class="card-body">
                    <h2 class="card-title">Post Title</h2>
                    <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis aliquid atque, nulla? Quos cum ex quis soluta, a laboriosam. Dicta expedita corporis animi vero voluptate voluptatibus possimus, veniam magni quis!</p>
                    <a href="#" class="btn btn-outline-dark">Read More &rarr;</a>
                </div>
                <div class="card-footer text-muted">
                    <i class="fa fa-clock-o"></i> Posted on January 1, 2017 by
                    <a href="#" class="text-info">Start Bootstrap</a>
                </div>
            </div>

            <!-- Pagination -->
            <ul class="pagination justify-content-center mb-4">
                <li class="page-item">
                    <a class="page-link" href="#">&larr; Older</a>
                </li>
                <li class="page-item disabled">
                    <a class="page-link" href="#">Newer &rarr;</a>
                </li>
            </ul>
        </div>

        <!-- side widget column -->
        <div class="col-sm-4">
            <!-- Search Widget -->
            <div class="card my-4">
                <h5 class="card-header bg-dark text-light">Search</h5>
                <div class="card-body">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Search for...">
                        <span class="input-group-btn">
                  <button class="btn btn-outline-dark" type="button">Go!</button>
                </span>
                    </div>
                </div>
            </div>
            <!-- Categories Widget -->
            <div class="card my-4">
                <h5 class="card-header bg-dark text-light">Categories</h5>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <ul class="list-unstyled mb-0">
                                <li>
                                    <a href="#" class="text-info">JavaScript</a>
                                </li>
                                <li>
                                    <a href="#" class="text-info">BootStrap</a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-sm-6">
                            <ul class="list-unstyled mb-0">
                                <li>
                                    <a href="#" class="text-info">PHP</a>
                                </li>
                                <li>
                                    <a href="#" class="text-info">HTML</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Side Widget -->
            <div class="card my-4">
                <h5 class="card-header bg-dark text-light">Side Widget</h5>
                <div class="card-body">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eos, molestiae neque. Eveniet iusto magnam minima, mollitia odio repellendus. Assumenda beatae consequuntur corporis cum doloribus in obcaecati, officiis repudiandae vel voluptates.
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Blog Posts End -->


<?php  include "assets/includes/footer.php"; ?>











































<!--<form class="container" id="needs-validation" novalidate>
    <div class="row">
        <div class="col-lg-6 col-sm-6 col-12">
            <div class="form-group">
                <label class="text-inverse" for="validationCustom01">First Name</label>
                <input type="text" class="form-control" id="validationCustom01" placeholder="First name" value="First Name" required>
            </div>
        </div>
        <div class="col-lg-6 col-sm-6 col-12">
            <div class="form-group">
                <label class="text-inverse" for="validationCustom02">Last Name</label>
                <input type="text" class="form-control" id="validationCustom02" placeholder="Last name" value="Last Name" required>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6 col-sm-12 col-12">
            <div class="form-group">
                <label class="text-inverse" for="inputEmail4">Email</label>
                <input type="email" class="form-control" id="inputEmail4" placeholder="Email" required>
                <div class="invalid-feedback">
                    Please provide a valid Email.
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-sm-6 col-12">
            <div class="form-group">
                <label class="text-inverse" for="inputpassword">Password</label>
                <input type="password" class="form-control" id="inputpassword" placeholder="password" required>
                <div class="invalid-feedback">
                    Please provide a valid password.
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6 col-sm-6 col-12">
            <div class="form-group">
                <label class="text-inverse" for="validationCustom03">City</label>
                <input type="text" class="form-control" id="validationCustom03" placeholder="City" required>
                <div class="invalid-feedback">
                    Please provide a valid city.
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-sm-6 col-12">
            <div class="form-group">
                <label class="text-inverse" for="select-menu">Select any value</label>
                <select class="custom-select d-block form-control" id="image" required>
                    <option value="">Open this select menu</option>
                    <option value="1">One</option>
                    <option value="2">Two</option>
                    <option value="3">Three</option>
                </select>
                <div class="invalid-feedback">
                    Please selected any option.
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6 col-sm-6 col-12">
            <div class="form-group">
                <label class="custom-file">
                    <input type="file" id="file" class="form-control custom-file-input" required>
                    <span class="custom-file-control"></span>
                    <div class="invalid-feedback">
                        Please selected any File.
                    </div>
                </label>
            </div>
        </div>
        <div class="col-lg-6 col-sm-6 col-12">
            <div class="form-group">
                <label class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" required>
                    <span class="custom-control-indicator"></span>
                    <span class="custom-control-description">Check this custom checkbox</span>
                </label>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6 col-sm-6 col-12">
            <div class="form-group">
                <label class="custom-control custom-radio">
                    <input id="radioStacked1" name="radio-stacked" type="radio" class="custom-control-input" required>
                    <span class="custom-control-indicator"></span>
                    <span class="custom-control-description">Toggle this custom radio</span>
                </label>
            </div>
        </div>
        <div class="col-lg-6 col-sm-6 col-12">
            <div class="form-group">
                <label class="custom-control custom-radio">
                    <input id="radioStacked2" name="radio-stacked" type="radio" class="custom-control-input" required>
                    <span class="custom-control-indicator"></span>
                    <span class="custom-control-description">Or toggle this other custom radio</span>
                </label>
            </div>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-lg-12 col-sm-12 col-12 text-center">
            <button class="btn btn-info" type="submit">Submit form</button>
        </div>
    </div>
</form>-->