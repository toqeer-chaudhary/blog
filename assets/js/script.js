$( document ).ready(function() {


        $("#first-name").blur(function () {
            validateFirstName();
        })

        $("#first-name").focus(function () {
            $("#first-name").css({
                "border":"0px",
                "background":"#fff",
            });
        })

        $("#email").blur(function () {
            validateEmail();
        })

        $("#email").focus(function () {
                $("#email").css({
                    "border":"0px",
                    "background":"#fff",
                });
            })



    // function to validate first_name

    function validateFirstName() {

        var firstName = $("#first-name").val();

        if(firstName == "" ||  (!/^[a-zA-Z]*$/g.test(firstName))) { // test ??

            $("#first-name").css({
                "border":"1px solid red",
            });

            $("#first-name-error").html("invalid first Name").css({
                "font-size":"15px",
                "color":"red",
            });

            return true;
        } else {

            $("#first-name").css({
                "border":"1px solid green",
            });

            $("#first-name-error").empty();
        }

        return false;
    }

    // validating email

    function validateEmail() {

        var email = $("#email").val();
        var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

        if(email == "" || (!mailformat.test(email))) { // test ??

            $("#email").css({
                "border":"1px solid red",
            });

            $("#email-error").html("invalid email address").css({
                "font-size":"15px",
                "color":"red",
            });

            return true;

        } else {

            $("#email").css({
                "border":"1px solid green",
            });

            $("#email-error").empty();
        }

        return false;
    }

    $("#user-form").submit(function () {
        if(validateFirstName() || validateEmail() ) {
            validateFirstName();
            validateEmail();
            return false;

        } else {

            alert("user added successfully");
            return true;
        }
    });

    /* =========================  image uploader script Begin =================================  */

    $(document).on('change', '.btn-file :file', function() {
        var input = $(this),
            label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
        input.trigger('fileselect', [label]);
    });

    $('.btn-file :file').on('fileselect', function(event, label) {

        var input = $(this).parents('.input-group').find(':text'),
            log = label;

        if( input.length ) {
            input.val(log);
        } else {
            if( log ) alert(log);
        }

    });
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#img-upload').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#imgInp").change(function(){
        readURL(this);
    });

    /* =========================  image uploader script End =================================  */

    /* =========================  form validation for post Begin =================================  */

        $("#post-title").blur(function () {
            validatePostTitle();
        })

        $("#post-title").focus(function () {
            $("#post-title").css({
                "border":"0px",
                "background":"#fff",
            });
        })

        $("#post-categories").blur(function () {
            validatePostCategories();

        })

        $("#post-categories").focus(function () {
            $("#post-categories").css({
                "border":"0px",
                "background":"#fff",
            });
        })

        $("#post-descriptioon").blur(function () {
            validatePostDescription();
        })

        $("#post-descriptioon").focus(function () {
            $("#post-descriptioon").css({
                "border":"0px",
                "background":"#fff",
            });
        })

    // function to validate first_name

    function validatePostTitle() {

        var postTitle = $("#post-title").val();

        if(postTitle == "") {

            $("#post-title").css({
                "border":"1px solid red",
            });

            $("#post-title-error").html("invalid first Name").css({
                "font-size":"15px",
                "color":"red",
            });

            return true;
        } else {

            $("#post-title").css({
                "border":"1px solid green",
            });

            $("#post-title-error").empty();
        }

        return false;
    }

        // function to validate first_name

        function validatePostCategories() {

            var postCategories = $("#post-categories").val();

            if(postCategories == "") {

                $("#post-categories").css({
                    "border":"1px solid red",
                });

                $("#post-categories-error").html("please select a image").css({
                    "font-size":"15px",
                    "color":"red",
                });

                return true;
            } else {

                $("#post-categories").css({
                    "border":"1px solid green",
                });

                $("#post-categories-error").empty();
            }

            return false;
        }

        // function to validate Post Description

        function validatePostDescription() {

            var postDescription = $("#post-descriptioon").val();

            if(postDescription == "") {

                $("#post-descriptioon").css({
                    "border":"1px solid red",
                });

                $("#post-description-error").html("invalid description").css({
                    "font-size":"15px",
                    "color":"red",
                });

                return true;
            } else {

                $("#post-descriptioon").css({
                    "border":"1px solid green",
                });

                $("#post-description-error").empty();
            }

            return false;
        }

    // function to validate Post image

    function validateImgIng() {

        var postImage = $("#imgInp").val();

        if(postImage == "") {

            $("#post-image-error").html("please select an image").css({
                "font-size":"15px",
                "color":"red",
            });

            return true;
        } else {

            $("#post-image-error").empty();
        }

        return false;
    }

    $("#post-form").submit(function () {
        if(validatePostTitle() || validatePostCategories() || validatePostDescription() || validateImgIng() ) {
            validatePostTitle();
            validatePostCategories();
            validatePostDescription();
            validateImgIng();
            return false;

        } else {

            alert("post created successfully");
            return true;
        }
    });

    /* =========================  form validation for post Emd =================================  */


    /* =========================  form validation for comments begin =================================  */

    $("#comment-form").submit(function () {

        $comment = $("#comment-area").val();
        if ($comment == "") {
            event.preventDefault();
            alert("please add a comment first");
        } else {
          //  event.preventDefault();
            alert("comment added successfully");
          /* /!* var c = (Math.floor((Math.random() * 4) + 1));
            var d = "image" + c;
            console.log(d);
            $("<div class='float-left'><img class='rounded-circle' src='assets/images/"+d+".jpg' width='64px' height='64px'></div>").appendTo("#comments");*!/*/
        }
    });

    /* =========================  form validation for comments end =================================  */

    $("#comment-description").blur(function () {
        validateCommentDescription();
    })

    $("#comment-description").focus(function () {
        $("#comment-description").css({
            "border":"0px",
            "background":"#fff",
        });
    })


    // function to validate Edit Comment Description

    function validateCommentDescription() {

        var commentDescription = $("#comment-description").val();

        if(commentDescription == "") {

            $("#comment-description").css({
                "border":"1px solid red",
            });

            $("#comment-description-error").html("invalid description").css({
                "font-size":"15px",
                "color":"red",
            });

            return true;
        } else {

            $("#comment-description").css({
                "border":"1px solid green",
            });

            $("#comment-description-error").empty();
        }

        return false;
    }

    $("#comment-edit-form").submit(function () {
         $commentDescription = $("#comment-description").val();
       // alert($commentDescription);
        if($commentDescription == "") {
            event.preventDefault();
            alert("please insert comment");
            validateCommentDescription();
        } else {
            alert("comment editted successfully");
        }


    });
});