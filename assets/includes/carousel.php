<div id="demo" class="carousel slide" data-ride="carousel">
    <ul class="carousel-indicators">
        <li data-target="#demo" data-slide-to="0" class="active"></li>
        <li data-target="#demo" data-slide-to="1"></li>
        <li data-target="#demo" data-slide-to="2"></li>
        <li data-target="#demo" data-slide-to="3"></li>
    </ul>
    <div class="carousel-inner">
        <div class="carousel-item active">
            <img src="assets/images/image3.jpg" alt="Image3" width="1100" height="200">
            <div class="carousel-caption">
                <h3>BLOG</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid?</p>
            </div>
        </div>
        <div class="carousel-item">
            <img src="assets/images/image1.jpg" alt="Image1" width="1100" height="500">
            <div class="carousel-caption">
                <h3>BLOG</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid?</p>
            </div>
        </div>
        <div class="carousel-item">
            <img src="assets/images/image2.png" alt="Image2" width="1100" height="500">
            <div class="carousel-caption">
                <h3>BLOG</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid?</p>
            </div>
        </div>
        <div class="carousel-item">
            <img src="assets/images/image4.png" alt="Image4" width="1100" height="500">
            <div class="carousel-caption">
                <h3>BLOG</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid?</p>
            </div>
        </div>
    </div>
    <a class="carousel-control-prev" href="#demo" data-slide="prev">
        <span class="carousel-control-prev-icon"></span>
    </a>
    <a class="carousel-control-next" href="#demo" data-slide="next">
        <span class="carousel-control-next-icon"></span>
    </a>
</div>
