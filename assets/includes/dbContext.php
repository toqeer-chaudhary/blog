<?php

    include "fileRead.php";

class DbContext
{

    private $host       ;
    private $username   ;
    private $password   ;
    private $database   ;
    private $message    ;
    private $connection ;

    //Class constructor
    public function __construct()
    {
        //initializing variables
        $this->host     = $GLOBALS["host"];
        $this->username = $GLOBALS["username"];
        $this->password = $GLOBALS["password"];
        $this->database = $GLOBALS["database"];

        //call open function
        $this->open_connection();
    }
    //Open function
    public function open_connection()
    {
        $this->connection = new mysqli($this->host, $this->username, $this->password, $this->database);


        if ($this->connection->connect_error) {
            die('Connect Error (' . mysqli_connect_errno() . ') ' . mysqli_connect_error());
        } else {
            echo $this->message ;
        }
    }

    //Close function
    public function close_connection()
    {
        if (isset($this->connection)) {
            mysqli_close($this->connection);
            unset($this->connection);
        }
    }

    //Select
    public function select($query)
    {

        $sql = mysqli_query($this->connection,$query);
        $rows = array();

        while ($row = mysqli_fetch_assoc($sql)) {
            $rows[] = $row;
        }
        return $rows;
    }

    //insert
    public function insert($query)
    {
        $insertRow = $this->connection->query($query);
        if($insertRow)
        {
            header("Location: post.php" );
            exit();
        }
        else
        {
            die("Your row is not inserted");
        }
    }

    //update
    public function update($query)
    {
        $updateRow = $this->connection->query($query);
        if($updateRow)
        {
            header("Location: fileRead.php?msg=".urlencode("Row Updated"));
            exit();
        }
        else
        {
            die("Your row is not Updated");
        }
    }

    //Delete
    public function delete($query)
    {
        $deleteRow = $this->connection->query($query);
       // echo $deleteRow;
        if($deleteRow)
        {
            header("Location: ../../post.php");
           // exit();
        }
        else
        {
            die("Your row is not Deleted");
        }
    }
}

?>