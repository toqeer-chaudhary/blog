<?php

    // reading data from .env file

    $myfile       = fopen(".env", "r") or die("Unable to open file!");
    $file         =  fread($myfile,filesize(".env"));
    fclose($myfile);

    // doing some manipulation with data to get desired results.

    $separation_1 = explode("\n",$file);
    $joining_1    = implode("=",$separation_1);
    $separtion_2  = explode("=",$joining_1);

    // initializing variables to be used in dbContext

    $host     = trim($separtion_2[1]);
    $username = trim($separtion_2[3]);
    $password = trim($separtion_2[5]);
    $database = trim($separtion_2[7]);

?>