<?php
    include "assets/includes/header.php";
    include "assets/includes/navbar.php";
    include "assets/includes/comments.php";

    $db = new Comments();

?>

<!-- Blog Posts Begin -->
<div class="container mt-5">
    <h2 class="my-4 text-center bg-dark text-light">BLOGS Are Awesome</h2>
    <div class="row">
        <div class="col-sm-8">
            <!-- Blog Post -->
            <div class="card mb-4">

                <div class="card-body">
                    <h2 class="card-title">Post Title</h2>
                    <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis aliquid atque, nulla? Quos cum ex quis soluta, a laboriosam. Dicta expedita corporis animi vero voluptate voluptatibus possimus, veniam magni quis!</p>
                    <a href="post.php?update=1&id=<?php echo $row["post_id"]; ?>" class="btn btn-outline-success" data-toggle="modal" data-target="#postModal" name="edit_post">Edit <i class="fa fa-pencil-square"></i></a>
                    <a href="assets/process/blogPost.php?delete=1&id=<?php echo $row["post_id"]; ?>" class="btn btn-outline-danger">Delete <i class="fa fa-trash"></i></a>
                </div>
                <div class="card-footer text-muted">
                    <i class="fa fa-clock-o"></i> Posted on January 1, 2017 by
                    <a href="#" class="text-info">Start Bootstrap</a>
                </div>
            </div>

            <!-- Comments Form -->
            <div class="card card-footer">
                <h4>Leave a Comment:</h4>
                <p class="error text-danger"><?php echo "$error"?></p>
                <form action="" method="post" id="comment-form">
                    <div class="form-group">
                        <textarea class="form-control" rows="3" id="comment-area" name="comment_description"></textarea>
                    </div>
                    <button type="submit" class="btn btn-outline-dark" id="submit-comment" name="submit_comment">Submit</button>
                </form>
            </div>


            <?php
                $myRows = $db->fetchComments();

                foreach ($myRows as $rows) {
                    ?>
                    <!-- Comment -->
            <div class="comments pt-2" id="comments">
                <div class="float-left mr-3">
                    <img class="" src="http://placehold.it/64x64" alt="">
                </div>
                <div class="">
                    <h4 class="">Commented at <i class="fa fa-clock-o"></i>
                    <small><?php echo $rows["commented_time"]; ?></small>
                     <!--   <a class="btn btn-outline-warning float-right" data-toggle="modal" data-target="#comment-modal" id="edit-comment" name="edit_comment" role="button"><i class="fa fa-pencil-square"></i></a>-->
                        <a onclick="return confirm('are you sure you want to delete it')" href="assets/includes/comments.php?id=<?=$rows['comment_id']?>" role="button" class="btn btn-outline-danger float-right" id="delete-comment" name="delete_comment"><i class="fa fa-trash"></i></a>
                    </h4>
                    <p><?php echo $rows["comment"]; ?></p>
                </div>
            </div>
                    <?php
                }

            ?>
        </div>

        <!-- side widget column -->
        <div class="col-sm-4">
            <!-- Search Widget -->
            <div class="card my-4">
                <h5 class="card-header bg-dark text-light">Search</h5>
                <div class="card-body">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Search for...">
                        <span class="input-group-btn">
                  <button class="btn btn-outline-dark" type="button">Go!</button>
                </span>
                    </div>
                </div>
            </div>
            <!-- Categories Widget -->
            <div class="card my-4">
                <h5 class="card-header bg-dark text-light">Categories</h5>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <ul class="list-unstyled mb-0">
                                <li>
                                    <a href="#" class="text-info">JavaScript</a>
                                </li>
                                <li>
                                    <a href="#" class="text-info">BootStrap</a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-sm-6">
                            <ul class="list-unstyled mb-0">
                                <li>
                                    <a href="#" class="text-info">PHP</a>
                                </li>
                                <li>
                                    <a href="#" class="text-info">HTML</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Side Widget -->
            <div class="card my-4">
                <h5 class="card-header bg-dark text-light">Side Widget</h5>
                <div class="card-body">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eos, molestiae neque. Eveniet iusto magnam minima, mollitia odio repellendus. Assumenda beatae consequuntur corporis cum doloribus in obcaecati, officiis repudiandae vel voluptates.
                </div>
            </div>
        </div>
    </div>
</div>

    <!-- Modal for editing comments-->
    <div class="modal fade" id="comment-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit Comment</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <form action="" method="get" id="comment-edit-form">
                        <div class="row">
                            <div class="form-group col-sm-12">
                                <label class="text-inverse" for="comment_description">Comment Description</label>
                                <input type="text">
                                <textarea class="form-control" rows="5" id="comment-description" name="comment_description" placeholder="Description"></textarea>
                                <p id="comment-description-error"></p>
                            </div>
                            <div class="modal-footer col-sm-12">
                                <button type="submit" class="btn btn-outline-dark" id="resubmit-comment">UPDATE</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal for users end-->

<?php  include "assets/includes/footer.php"; ?>